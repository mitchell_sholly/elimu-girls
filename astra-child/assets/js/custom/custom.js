"use strict";

(function ($) {

	$(document).ready(function ($) {

		if ($("body").hasClass("single-give_forms postid-2264")) {
            // Set Amount to 0 and apply focus to force any validation
			$("#give-amount").val("0").focus();
		}

	});

})(jQuery);
