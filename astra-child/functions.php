<?php

/**
 * Astra Child Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Astra Child
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define('CHILD_THEME_ASTRA_CHILD_VERSION', '1.0.0');

/**
 * Enqueue styles
 */
function child_enqueue_styles()
{

    wp_enqueue_style('astra-child-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), filemtime(get_stylesheet_directory() . '/style.min.css'));
    wp_enqueue_script('astra-child-theme-js', get_stylesheet_directory_uri() . '/assets/js/custom.js', array('jquery'), filemtime(get_stylesheet_directory() . '/assets/js/custom.js'), true);
}

add_action('wp_enqueue_scripts', 'child_enqueue_styles', 15);

/**
 * Change Columns for Single Product Gallery
 */
// add_filter( 'woocommerce_product_thumbnails_columns', 'elimu_change_product_thumbnails_columns' );
function elimu_change_product_thumbnails_columns()
{
    return 3; // change the value as per your need
}

/**
 * Change No Shipping Option Message
 */
add_filter('woocommerce_cart_no_shipping_available_html', 'change_noship_message');
add_filter('woocommerce_no_shipping_available_html', 'change_noship_message');
function change_noship_message()
{
    echo "<p class='no-intl-shipping'>For International Shipping options please email <a href='mailto:cindy@elimugirls.com'>cindy@elimugirls.com</a>.</p>";
}

/**
 * Change "Out Of Stock" text added on WooCommerce Product Single, non-astra layouts.
 *
 * @return String
 */
add_filter('woocommerce_get_availability', 'elimu_custom_get_availability', 1, 2);
function elimu_custom_get_availability($availability, $_product)
{
    if (!$_product->backorders_allowed()) {
        if (!$_product->is_in_stock()) $availability['availability'] = __('SOLD!', 'woocommerce');
    }
    
    return $availability;
}


/**
 * Change "Out Of Stock" text added on WooCommerce Product Grid for Astra Layouts.
 *
 * @return String
 */
function elimu_prefix_change_out_stock_string() {
    global $product;
    if (!$product->backorders_allowed()) {
        return __( 'SOLD!', 'your-text-domain' );
    }
}
add_filter( 'astra_woo_shop_out_of_stock_string', 'elimu_prefix_change_out_stock_string' );